from rest_framework import status
from rest_framework.test import APITestCase

from companies.models import Company
from users.consts import MANAGER
from users.models import User


class JwtAuthAPITestCase(APITestCase):
    jwt_auth_url = '/api/jwt-auth/auth/'
    users_url = '/api/users/'
    companies_url = '/api/companies/'

    def setUp(self):
        self.company = Company.objects.create(name='test company', email='test@email.com')
        self.user = User.objects.create_admin_user(username='admin', password='test1234', company=self.company)

    def test_jwt_auth_endpoint(self):
        credentials = {'username': 'admin', 'password': 'test1234'}
        response = self.client.post(self.jwt_auth_url, data=credentials)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(response.data.get('token')) > 10)

        wrong_credentials = {'username': 'admin123', 'password': 'test1234'}
        response = self.client.post(self.jwt_auth_url, data=wrong_credentials)
        self.assertTrue('Unable to log in with provided credentials.' in str(response.data))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_make_request_to_endpoints_using_jwt(self):
        # get token
        credentials = {'username': 'admin', 'password': 'test1234'}
        response = self.client.post(self.jwt_auth_url, data=credentials)
        token = response.data.get('token')

        # check POST method for creating company
        data = {'name': 'new company using JWT'}
        response = self.client.post(self.companies_url, data=data, HTTP_AUTHORIZATION='JWT {}'.format(token))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Company.objects.filter(name='new company using JWT').exists())

        # check POST method for creating user
        data = {'username': 'jwtuser', 'password': 'jwtpassword123', 'role': MANAGER, 'company': self.company.pk}
        response = self.client.post(self.users_url, data=data, HTTP_AUTHORIZATION='JWT {}'.format(token))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(User.objects.filter(username='jwtuser').exists())
