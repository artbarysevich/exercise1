from django.conf.urls import url, include

from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework_swagger.views import get_swagger_view

from api.v1.companies.views import CompanyCreateView
from api.v1.init.views import InitCompaniesAPIView, InitUsersAPIView
from api.v1.users.views import UserCreateUpdateDestroyView


schema_view = get_swagger_view(title='Exercise API - v1')

router = routers.DefaultRouter()
router.register(r'users', UserCreateUpdateDestroyView)
router.register(r'companies', CompanyCreateView)

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^', include(router.urls)),

    url(r'^jwt-auth/auth/', obtain_jwt_token),
    url(r'^jwt-auth/refresh/', refresh_jwt_token),
    url(r'^api-auth/', include('rest_framework.urls')),

    url(r'^init/companies/', InitCompaniesAPIView.as_view(), name='init_companies'),
    url(r'^init/users/', InitUsersAPIView.as_view(), name='init_users'),
]
