from rest_framework import serializers

from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.password_validation import validate_password

from users.consts import ADMIN, MANAGER
from users.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'password', 'role', 'company')

    def validate_role(self, role):
        if role not in [ADMIN, MANAGER]:
            raise serializers.ValidationError(_('Can be used only Admin or Manager role'))
        return role

    def validate_password(self, password):
        validate_password(password)
        return password
