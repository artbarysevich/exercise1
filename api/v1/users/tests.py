from rest_framework import status
from rest_framework.test import APITestCase

from companies.models import Company
from users.consts import MANAGER, ADMIN, SUPPORT
from users.models import User


class UserAPITestCase(APITestCase):

    def setUp(self):
        self.user_url = '/api/users/'
        self.user_detail_url = '/api/users/{}/'
        self.company = Company.objects.create(name='test company', email='test@email.com')

        self.admin_user = User.objects.create_admin_user(
            username='admin', password='test1234', company=self.company)
        self.manager_user = User.objects.create_manager_user(
            username='manager', password='test1234', company=self.company)
        self.support_user = User.objects.create_support_user(
            username='support', password='test1234', company=self.company)

    def test_check_access_to_users_endpoints_without_credentials(self):
        response = self.client.post(self.user_url)  # POST list
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.put(self.user_detail_url.format(self.admin_user.pk))  # PUT detail
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(self.user_detail_url.format(self.admin_user.pk))  # PATCH detail
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.delete(self.user_detail_url.format(self.admin_user.pk))  # DELETE detail
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_admin_can_create_new_admin_and_manager_users(self):
        self.client.login(username='admin', password='test1234')

        data = {'username': 'newmanager', 'password': 'newpassword1234', 'role': MANAGER, 'company': self.company.pk}
        response = self.client.post(self.user_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        new_manager = User.objects.get(username='newmanager')
        self.assertEqual(new_manager.role, MANAGER)
        self.assertNotEqual(new_manager.password, 'newpassword1234')
        self.assertTrue(new_manager.check_password('newpassword1234'))

        data = {'username': 'newadmin', 'password': 'newpassword1234', 'role': ADMIN, 'company': self.company.pk}
        response = self.client.post(self.user_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        new_admin = User.objects.get(username='newadmin')
        self.assertEqual(new_admin.role, ADMIN)
        self.assertNotEqual(new_admin.password, 'newpassword1234')
        self.assertTrue(new_admin.check_password('newpassword1234'))

    def test_admin_can_update_and_delete_admin_and_manager_users(self):
        self.client.login(username='admin', password='test1234')

        # try update admin
        data = {
            'username': self.admin_user.username,
            'password': 'newpassword4321',
            'role': self.admin_user.role,
            'company': self.company.pk
        }
        response = self.client.put(self.user_detail_url.format(self.admin_user.pk), data=data)
        updated_user = User.objects.get(pk=self.admin_user.pk)
        self.assertTrue(updated_user.check_password('newpassword4321'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()
        self.client.login(username=self.admin_user.username, password='newpassword4321')

        # try delete manager
        deleted_user_pk = self.manager_user.pk
        response = self.client.delete(self.user_detail_url.format(self.manager_user.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(User.objects.filter(pk=deleted_user_pk).count(), 0)

    def test_admin_cant_create_support_users(self):
        self.client.login(username='admin', password='test1234')

        data = {'username': 'newmanager', 'password': 'newpassword1234', 'role': SUPPORT, 'company': self.company.pk}
        response = self.client.post(self.user_url, data)
        self.assertTrue('Can be used only Admin or Manager role' in str(response.data.get('role')))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_admin_cant_update_and_delete_support_users(self):
        self.client.login(username='admin', password='test1234')

        # try update support user -> should be error 404 not found
        response = self.client.put(self.user_detail_url.format(self.support_user.pk), data={})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # try delete support user -> should be error 404 not found
        response = self.client.delete(self.user_detail_url.format(self.support_user.pk))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_manager_and_support_cant_create_and_update_and_delete_users(self):
        self.client.login(username='manager', password='test1234')

        # try create user as manager
        response = self.client.post(self.user_url, data={})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # try update user as manager
        response = self.client.put(self.user_detail_url.format(self.support_user), data={})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # try delete user as manager
        response = self.client.delete(self.user_detail_url.format(self.manager_user))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.client.logout()
        self.client.login(username='support', password='test1234')

        # try create user as support
        response = self.client.post(self.user_url, data={})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # try update user as support
        response = self.client.put(self.user_detail_url.format(self.manager_user), data={})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # try delete user as support
        response = self.client.delete(self.user_detail_url.format(self.manager_user))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

