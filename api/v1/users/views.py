from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from api.v1.mixins import RequiredRolesMixin
from api.v1.users.serializers import UserSerializer
from users.consts import ADMIN
from users.models import User


class UserCreateUpdateDestroyView(RequiredRolesMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin,
                                  mixins.DestroyModelMixin, GenericViewSet):
    queryset = User.objects.admins_and_managers()
    serializer_class = UserSerializer
    required_roles = (ADMIN,)

    def perform_create(self, serializer):
        User.objects.create_user(**serializer.validated_data)

    def perform_update(self, serializer):
        password = serializer.validated_data.get('password')
        user = serializer.save()
        # set new hashed password to user if we got it in PUT/PATCH requests
        if password:
            user.set_password(password)
            user.save()

