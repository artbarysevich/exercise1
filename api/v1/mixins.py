
class RequiredRolesMixin(object):
    """
    Mixin allows access to endpoint methods if user have required role
    """
    required_roles = []
    required_create_roles = []
    required_update_roles = []
    required_get_roles = []
    required_destroy_roles = []

    def dispatch(self, request, *args, **kwargs):
        """
        `self.http_method_names = []` - returns exception `405. Method not allowed.` for any requests
        """

        if not self.request.user.is_authenticated:
            """
            Ignore roles if user is not authenticated.
            DRF calls automatically `401. Unauthorized.` exception if it needed.
            """
            return super(RequiredRolesMixin, self).dispatch(request, *args, **kwargs)

        user_role = self.request.user.role

        if user_role in self.required_roles:
            pass
        elif request.method == 'POST' and user_role not in self.required_create_roles:
            self.http_method_names = []
        elif request.method in ('PUT', 'PATCH') and user_role not in self.required_update_roles:
            self.http_method_names = []
        elif request.method == 'DELETE' and user_role not in self.required_destroy_roles:
            self.http_method_names = []
        elif request.method == 'GET' and user_role not in self.required_get_roles:
            self.http_method_names = []

        return super(RequiredRolesMixin, self).dispatch(request, *args, **kwargs)
