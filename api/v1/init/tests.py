from rest_framework import status
from rest_framework.test import APITestCase

from companies.models import Company
from users.models import User


class InitAPITestCase(APITestCase):

    def setUp(self):
        company = Company.objects.create(name='test company', email='test@email.com')
        User.objects.create_admin_user(username='admin', password='test1234', company=company)

        self.init_companies_url = '/api/init/companies/'
        self.init_users_url = '/api/init/users/'
        self.client.login(username='admin', password='test1234')

    def test_init_companies(self):
        response = self.client.get(self.init_companies_url)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Company.objects.filter(name='ABC').exists())
        self.assertTrue(Company.objects.filter(name='Support').exists())

    def test_init_users(self):
        self.client.get(self.init_companies_url)  # init companies
        response = self.client.get(self.init_users_url)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(User.objects.filter(username='bob').exists())
        self.assertTrue(User.objects.filter(username='alice').exists())
        self.assertTrue(User.objects.filter(username='frank').exists())
