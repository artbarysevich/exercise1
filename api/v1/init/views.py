from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.utils.translation import ugettext_lazy as _

from companies.models import Company
from users.models import User


class InitCompaniesAPIView(APIView):
    """
    Init script for creating companies:
        - Support
        - ABC
    """

    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        Company.objects.get_or_create(name='Support')
        Company.objects.get_or_create(name='ABC')
        return Response(status=status.HTTP_201_CREATED)


class InitUsersAPIView(APIView):
    """
    Init script for creating users:
        - Bob (support)
        - Alice (admin)
        - Frank (manager)
    """

    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):

        try:
            support_company = Company.objects.get(name='Support')  # .get_or_create() ???
            if not User.objects.filter(username='bob').exists():
                User.objects.create_support_user(username='bob', password='bobpassword', company=support_company)
        except Company.DoesNotExist:
            return Response(
                _('Company `Support` does not exist. Try execute `init companies`'), status=status.HTTP_400_BAD_REQUEST)

        try:
            abc_company = Company.objects.get(name='ABC')  # .get_or_create() ???
            if not User.objects.filter(username='alice').exists():
                User.objects.create_admin_user(username='alice', password='alicepassword', company=abc_company)
            if not User.objects.filter(username='frank').exists():
                User.objects.create_manager_user(username='frank', password='frankpassword', company=abc_company)
        except Company.DoesNotExist:
            return Response(
                _('Company ABC does not exist. Try execute `init companies`'), status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_201_CREATED)
