from rest_framework import status
from rest_framework.test import APITestCase

from companies.models import Company
from users.models import User


class CompanyAPITestCase(APITestCase):

    def setUp(self):
        self.company_url = '/api/companies/'
        self.company_detail_url = '/api/companies/{}/'
        self.company = Company.objects.create(name='test company', email='test@email.com')

        User.objects.create_admin_user(username='admin', password='test1234', company=self.company)
        User.objects.create_manager_user(username='manager', password='test1234', company=self.company)
        User.objects.create_support_user(username='support', password='test1234', company=self.company)

    def test_check_access_to_companies_endpoints(self):
        response = self.client.post(self.company_url)  # POST list
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.put(self.company_detail_url.format(self.company.pk))  # PUT detail
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(self.company_detail_url.format(self.company.pk))  # PATCH detail
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_only_support_user_can_create_company(self):
        self.client.login(username='support', password='test1234')
        data = {'name': 'support company name'}
        response = self.client.post(self.company_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_admin_and_manager_cant_create_company(self):
        # admin
        self.client.login(username='admin', password='test1234')
        data = {'name': 'admin company name'}
        response = self.client.post(self.company_url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.client.logout()

        # manager
        self.client.login(username='manager', password='test1234')
        data = {'name': 'manager company name'}
        response = self.client.post(self.company_url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_support_user_post_method_companies_validation(self):
        self.client.login(username='support', password='test1234')
        response = self.client.post(self.company_url, data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('This field is required.' in str(response.data.get('name')))

    def test_admin_user_can_edit_all_company_fields_except_name_field(self):
        self.client.login(username='admin', password='test1234')
        data = {'name': 'new company name', 'email': 'new@email.com', 'phone': '+123456789'}
        response = self.client.put(self.company_detail_url.format(self.company.pk), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('email'), data.get('email'))
        self.assertEqual(response.data.get('phone'), data.get('phone'))
        self.assertNotEqual(response.data.get('name'), data.get('name'))
        company = Company.objects.get(pk=self.company.pk)
        self.assertEqual(company.email, data.get('email'))
        self.assertNotEqual(company.name, data.get('name'))

    def test_manager_and_support_cant_edit_company(self):
        data = {'name': 'new company name', 'email': 'new@email.com', 'phone': '+123456789'}

        # manager
        self.client.login(username='manager', password='test1234')
        response = self.client.put(self.company_detail_url.format(self.company.pk), data=data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.client.logout()

        # admin
        self.client.login(username='support', password='test1234')
        response = self.client.put(self.company_detail_url.format(self.company.pk), data=data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
