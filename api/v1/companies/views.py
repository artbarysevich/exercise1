from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from api.v1.companies.serializers import CompanySerializer
from api.v1.mixins import RequiredRolesMixin
from companies.models import Company
from users.consts import SUPPORT, ADMIN


class CompanyCreateView(RequiredRolesMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    required_create_roles = (SUPPORT,)
    required_update_roles = (ADMIN,)

    def perform_update(self, serializer):
        # Admin user can't update name field
        serializer.validated_data.pop('name', None)
        serializer.save()

