from django.contrib.auth.models import UserManager
from django.db.models import Q

from users.consts import ADMIN, MANAGER, SUPPORT


class CustomUserManager(UserManager):

    def admins_and_managers(self):
        return self.get_queryset().filter(Q(role=ADMIN) | Q(role=MANAGER))

    def _create_user(self, username, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        username = self.model.normalize_username(username)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        return self._create_user(username, password, **extra_fields)

    def create_admin_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('role', ADMIN)
        return self._create_user(username, password, **extra_fields)

    def create_manager_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('role', MANAGER)
        return self._create_user(username, password, **extra_fields)

    def create_support_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('role', SUPPORT)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        """
        Ignore command bcoz we do not have `is_superuser` field in User model
        """
        raise NotImplementedError('Project do not have superusers accounts')

