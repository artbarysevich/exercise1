from django.test import TestCase

from companies.models import Company
from users.consts import ADMIN, MANAGER, SUPPORT
from users.models import User


class UserTestCase(TestCase):

    def setUp(self):
        self.company = Company.objects.create(name='test company')

    def test_user_model_creating(self):
        user = User.objects.create_user(username='user', password='password1234', company=self.company, role=ADMIN)
        self.assertNotEqual(user.password, 'password1234')  # password should be hashed in database
        self.assertTrue(user.check_password('password1234'))  # checks that password generated correctly
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.first().username, user.username)

    def test_user_model_admin_creating(self):
        user_admin = User.objects.create_admin_user(username='useradmin', password='password4321', company=self.company)
        self.assertNotEqual(user_admin.password, 'password4321')
        self.assertTrue(user_admin.check_password('password4321'))
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.first().username, user_admin.username)
        self.assertEqual(User.objects.first().role, ADMIN)

    def test_user_model_manager_creating(self):
        user_manager = User.objects.create_manager_user(
            username='usermanager', password='password432156', company=self.company)
        self.assertNotEqual(user_manager.password, 'password432156')
        self.assertTrue(user_manager.check_password('password432156'))
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.first().username, user_manager.username)
        self.assertEqual(User.objects.first().role, MANAGER)

    def test_user_model_support_creating(self):
        user_support = User.objects.create_support_user(
            username='usersupport', password='password432133', company=self.company)
        self.assertNotEqual(user_support.password, 'password432133')
        self.assertTrue(user_support.check_password('password432133'))
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.first().username, user_support.username)
        self.assertEqual(User.objects.first().role, SUPPORT)

    def test_user_queryset_method_admins_and_managers(self):
        User.objects.create_support_user(username='usersupport1', password='password12341', company=self.company)
        User.objects.create_support_user(username='usersupport2', password='password12342', company=self.company)
        self.assertEqual(User.objects.admins_and_managers().count(), 0)

        User.objects.create_admin_user(username='useradmin1', password='password12343', company=self.company)
        User.objects.create_admin_user(username='useradmin2', password='password12344', company=self.company)
        self.assertEqual(User.objects.admins_and_managers().count(), 2)

        User.objects.create_manager_user(username='usermanager1', password='password12345', company=self.company)
        User.objects.create_manager_user(username='usermanager2', password='password12346', company=self.company)
        self.assertEqual(User.objects.admins_and_managers().count(), 4)

