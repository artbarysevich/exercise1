from django.utils.translation import ugettext_lazy as _


ADMIN = 1
MANAGER = 2
SUPPORT = 3

ROLE_CHOICES = (
    (ADMIN, _('Admin')),
    (MANAGER, _('Manager')),
    (SUPPORT, _('Support'))
)