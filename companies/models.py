from django.db import models
from django.utils.translation import ugettext_lazy as _


class Company(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    phone = models.CharField(_('Phone'), max_length=64, blank=True, null=True)
    email = models.EmailField(_('Email'), max_length=128, blank=True, null=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def __str__(self):
        return self.name
