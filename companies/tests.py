from django.test import TestCase

from companies.models import Company


class CompanyTestCase(TestCase):

    def test_company_model_creating(self):
        company = Company.objects.create(name='company name #1', phone='+48134456789', email='comp@comp.com')
        self.assertEqual(Company.objects.count(), 1)
        self.assertEqual(Company.objects.first().name, company.name)


